# Reescribe el programa del salario usando try y except, de modo que el programa sea capaz de gestionar
# entradas no numéricas con elegancia, mostrando un mensaje y saliendo del programa.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
try:
    horas = int(input("Introduzca las horas: "))
    tarifa = float(input("Introduzca la tarifa por hora: "))
    salario = (horas * tarifa)
    if horas > 40:
        horas = salario + (tarifa * 1.5)
        salario = horas + tarifa
        print("Es salario es: \n", salario)
    else:
        salario = (horas * tarifa)
        print("Es salario es: \n", salario)
except:
    print("Error, por favor introduzca un número")
