# Programa que solicite una puntuación entre 0.0 y 1.0. Si la puntuación está fuera de ese rango, muestra
# un mensaje de error.
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
try:
    puntuación = float(input("Intoduzca puntuación: "))
    if puntuación >= 0.0 and puntuación <= 1.0:
        if puntuación >= 0.9:
            print ("Sobresaliente")
        elif puntuación >= 0.8:
            print ("Notable")
        elif puntuación >= 0.7:
            print ("Bien")
        elif puntuación >= 0.6:
            print ("Suficiente")
        elif puntuación < 0.6:
            print ("Insuficiente")
    else:
        print ("Puntuación incorrecta")
except:
    print("Puntuación incorrecta")
